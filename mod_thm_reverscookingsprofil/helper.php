<?php

// No direct access to this file
defined('_JEXEC') or die;



class THMReversCookings
{
	static  $attribs;
	
	public function  __construct(){
		$this->attribs['width'] = '70px';
		$this->attribs['height'] = '70px';
	}
	//check , if the User has a profil
	public static  function hasprofil($userid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from(' #__thm_reverscookingsprofils');
		$query->where('created_by ='.$userid);
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if(empty($result))
			return null;
		return $result;
	}
	
	public static function hasbild($datenbanktabel,$element, $element_value){
		$db = JFactory::getDbo();
		$query = "SELECT * FROM ".$datenbanktabel." WHERE ".$element." = '$element_value'";
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if(empty($result))
			return false;
		return $result;
	
	}
	public static function myprofil($userid, $element , $ismyprofil){
	$db = JFactory::getDbo();
	$query = 'SELECT * FROM  #__thm_reverscookingsprofils WHERE '.$element.' ='.$userid;
	$db->setQuery($query);
	$userdata = $db->loadObjectList();
	$image = self::hasbild('#__thm_reverscookingsprofils_bild', 'profilid', $userdata[0]->id);
	$result='<div>';
	$result .= '<div><h4>'.$userdata[0]->uname.'</h4></div>';
	$result .= '<div><a href='.JRoute::_('index.php/component/uddeim/?task=new&Itemid=0').'>'.JText::_('COM_Cook_With_Me').'!</a></div>';
	$result .= '<div>'.JHTML :: image('components/com_thm_reverscookings/img/profil_bild/'.$image[0]->filename, $userdata[0]->uname."_Bild", self::$attribs).'<a href="'.JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&layout=edit&id='.$userid).'"></a></div>';
	if($ismyprofil == false)
	$result .='<div><h4> '.JText::_('COM_Adresse').' :  '.$userdata[0]->address.'<br> '.$userdata[0]->plz.' '.$userdata[0]->ort.'</h4></div>';
	$result .='</div>';
	return $result;
		
	}
	
	public static function  getprofilid($userid){
		$db = JFactory::getDbo();
		$query = 'SELECT id FROM #__thm_reverscookingsprofils WHERE created_by ='.$userid;
		$db->setQuery($query);
		$userdata = $db->loadObjectList();
		return intval($userdata[0]->id);
	}
	
	public static function suchzutaten($rezeptid, $created_by){
		$profilid = self::getprofilid($created_by);
		$db = JFactory::getDbo();
		$query = 'select a.ingid from #__thm_reverscookings_ingredients_rezept as a where a.rezeptid ='.$rezeptid.' and a.ingid 
					not in (SELECT b.ingid from #__thm_reverscookings_virtual_fridge as b where b.userid ='.$profilid.')';
		
		$db->setQuery($query);
		$ingdata = $db->loadObjectList();
		$whererequest = 'a.ingid=';
		for ($i=0; $i<count($ingdata); $i++){
			$ing=$ingdata[$i];
			$whererequest .=$ing->ingid;
			if($i< count($ingdata)-1)
				$whererequest.=' OR a.ingid=';
		}
		$userquery = 'SELECT  a.userid as userid  ,a.ingid as ingid,b.ingname as ingname
				FROM #__thm_reverscookings_virtual_fridge AS a LEFT JOIN #__thm_reverscookings_ingredients AS b ON a.ingid = b.id  WHERE a.userid <>'.$profilid.' AND ('.$whererequest.' )';
		$db->setQuery($userquery);
		$db->query();
		$userlist = $db->loadObjectList();
		if(count($userlist)==0)
			return null;
		$userdata = array();
		$inganzahl = count($ingdata)/2;
		$result = array();
		for($y =0; $y<count($userlist);$y++){
			$userid = $userlist[$y]->userid;
			$inglist = array();
			$in =$y;
			$treffer=0;
			while($in<count($userlist) && $userlist[$in]->userid == $userid  ){
			
				array_push($inglist,$userlist[$in]->ingname);
				$treffer++;
				$in++;
				
			}
			
			if($treffer >= $inganzahl){
				$element = new  stdClass();
				$element->userid = $userid;
				$element->ing = $inglist;
				$element->treffer = $treffer;
				array_push($result, $element);
			}
			if($in == count($userlist))
				break;
			$y = $in-1;
			
		}
		
		if(count($result)==0)
			return null;
		$rands = 0;
		if(count($result)>1){
		$max = count($result)-1;
		$rands = rand(0, $max);
		}
		return $result[$rands];
	}
}
